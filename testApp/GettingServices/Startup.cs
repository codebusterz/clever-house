﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using DependencyInjection;
using DependencyInjection.Services;

namespace GettingServices
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IMessageSender, EmailMessageSender>();
            services.AddTransient<TimeService>();
            services.AddTransient<MessageService>();
            services.AddTransient<SmsMessageSender>();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, TimeService timer)
        {
            
            app.Run(async (context) =>
            {
                SmsMessageSender send =  context.RequestServices.GetService<SmsMessageSender>();

                MessageService sender = context.RequestServices.GetRequiredService<MessageService>();
                await context.Response.WriteAsync($"{sender.SendMessage()}" + $" at {timer.GetTime()}" + $"{send.Send()}");
            });
        }
    }
}
