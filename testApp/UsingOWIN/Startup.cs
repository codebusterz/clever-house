﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace UsingOWIN
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
         
            app.Run(async (context) => 
            {
                app.UseOwin(pipeline =>
                {
                    pipeline(next => SendResponseAsync);
                });
            });
        }
        public Task SendResponseAsync(IDictionary<string,object> env)
        {
            //aswer detection
            string responseText = "Hello ASP.Net core";
            //coding into byte array
            byte[] responseBytes = Encoding.UTF8.GetBytes(responseText);
            //getting answer
            var responseStream = (Stream)env["owin.ResponseBody"];
            //sending answer
            return responseStream.WriteAsync(responseBytes, 0, responseBytes.Length);
        }
    }
}
