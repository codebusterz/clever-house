﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LiveCycle.Services;
using Microsoft.AspNetCore.Http;

namespace LiveCycle
{
    public class CounterMiddleware
    {
        private int i = 0;
        RequestDelegate _next;
        public CounterMiddleware(RequestDelegate next)
        {
            _next = next;
        }
        public async Task InvokeAsync(HttpContext context, ICounter counter, CounterService counterService)
        {
            ++i;
            await context.Response.WriteAsync($"Request:{i}  \nIcounter:{counter.Value}" + $"\nCounterService:{counterService.Counter.Value}");
            await _next(context);
        }
    }
}
