﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LiveCycle.Services
{
    public interface ICounter
    {
       int Value { get; }
    }
    public class RandomCounter:ICounter
    {
        private readonly int value;
        static readonly Random rnd = new Random();
        public RandomCounter()
        {
            value = rnd.Next(0, 100000);
        }
        public int Value { get { return value; } } 
    }
    public class CounterService
    {
        public ICounter Counter { get;  }
        public CounterService(ICounter counter)
        {
            Counter = counter;
        }
    }
}
