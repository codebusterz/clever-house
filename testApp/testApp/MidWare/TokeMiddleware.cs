﻿//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Builder;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
////Middleware:
////1. Construct with RequestDelegate parametr. it is a pointer on next function
////2. Invoke or InvokeAsync
//namespace testApp
//{
//    public class TokeMiddleware
//    {
//        RequestDelegate _next;
//        string _pattern;
//        public TokeMiddleware(RequestDelegate next, string pattern)
//        {
//            _pattern = pattern;
//            _next = next;
//        }
//        //Почемуу метод должен называться именно так
//        public async Task InvokeAsync(HttpContext context)
//        {
//            var Token = context.Request.Query["Token"];
//            if (Token != _pattern)
//            {
//                context.Response.StatusCode = 403;
//                await context.Response.WriteAsync("Token is invalid");
//            }
//            else await _next(context);
//        }

//    }

//}
