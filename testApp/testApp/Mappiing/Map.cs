﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using Microsoft.AspNetCore.Builder;
//using Microsoft.AspNetCore.Http;

//namespace testApp
//{
//    public class Startup
//    {
//        public void Configure(IApplicationBuilder app)
//        {
//            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
//            app.Map("/home", home =>
//            {
//                home.Map("/index", Index);                
//                home.Map("/about", About);
//            });
//            app.MapWhen(context => {
//                return context.Request.Query.ContainsKey("id") && 
//                context.Request.Query["id"] == "5"; },HandleId );
//            app.Run(async (context) =>{ context.Response.ContentType = "text/html;charset = utf-8"; await context.Response.WriteAsync("<h2>Not Found</h2>");});
//        }
//        private static void HandleId(IApplicationBuilder app)
//        {
//            app.Run(async context =>
//            {
//                await context.Response.WriteAsync("id is equal to 5");
//            });
//        }
//        private void About(IApplicationBuilder app)
//        {
//            app.Run(async (context) => { context.Response.ContentType = "text/html;charset = utf-8"; await context.Response.WriteAsync("<h2>About</h2>"); });               
//        }
//        private void Index(IApplicationBuilder app)
//        {           
//            app.Run(async (context) => { context.Response.ContentType = "text/html;charset = utf-8"; await context.Response.WriteAsync("<h2>Index</h2>"); });
//        }
//    }
//}
